import { items, sidebar } from "./data.js";
import shapeDot from "./assets/icon/Dotted Shape.svg";
import { ArtBlog } from "./components/ArtBlog";
import { Blog } from "./components/Blog";
import { SideBar } from "./components/SideBar.jsx";

function App() {
    return (
        <div className="w-[1170px] container mx-auto">
            <Blog />
            <div class="grid grid-rows-2 grid-flow-col gap-[30px] mt-[80px]">
                <div class="row-span-2 col-span-2 flex flex-col w-[770px]">
                    <ArtBlog data={items} />
                </div>
                <div class="row-span-2 w-[370px] relative">
                    <SideBar data={sidebar} />
                    <img
                        className="absolute top-0 right-0"
                        src={shapeDot}
                        alt=""
                    />
                </div>
            </div>
        </div>
    );
}

export default App;
