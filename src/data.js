import image1 from "./assets/img/Rectangle 4367.jpg";
import image2 from "./assets/img/Rectangle 4367 (1).jpg";
import arrowRight from "./assets/icon/arrow-right.svg";

export const items = [
    {
        img: image1,
        subject: "Digital Marketing",
        title: "How to use Facebook ads to sell online courses",
        desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        icon: arrowRight,
    },
    {
        img: image2,
        subject: "Digital Marketing",
        title: "The Data-Driven Approach to Understanding.",
        desc: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
        icon: arrowRight,
    },
];

export const sidebar = [
    {
        subject: "Graphics Design",
        title: "Design is a Plan or The Construction of an Object.",
        desc: "Lorem Ipsum is simply dummy text of the printing and industry page when looking at its layout.",
        icon: arrowRight,
    },
    {
        subject: "Digital Marketing",
        title: "How to use Facebook ads to sell online courses.",
        desc: "Lorem Ipsum is simply dummy text of the printing and industry page when looking at its layout.",
        icon: arrowRight,
    },
];
