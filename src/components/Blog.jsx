export const Blog = () => {
    return (
        <div className="flex flex-col items-center mt-[120px]">
            <p className="text-[#3056D3] text-[18px] leading-[24px] font-[600]">Our Blogs</p>
            <h1 className="text-[#2E2E2E] text-[40px] leading-[45px] font-[700] mt-[8px]">Our Recent News</h1>
            <div className="w-[770px] text-center mt-[15px]">
            <p className="text-[#637381] text-[16px] leading-[25px] font-[400] ps-[130px] pe-[130px]">There are many variations of passages of Lorem Ipsum available
but the majority have suffered alteration in some form.</p>
            </div>            
        </div>
    )
}