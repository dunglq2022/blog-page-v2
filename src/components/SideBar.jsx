export const SideBar = ({data}) => {
    console.log(data)
    return(
        <>
            {
                data.map((item, index) => (
                    <div key={index} className="bg-[#3056D3] p-[30px]">
                        <span className="text-[#FFFFFF] text-[14px] leading-[20px] font-[600] tracking-[0.5px]">{item.subject}</span>
                        <h2 className="mt-[7px] text-[#FFFFFF] text-[22px] leading-[30px] font-[600]">{item.title}</h2>
                        <p className="mt-[12px] text-[#EBEBEB] text-[16px] leading-[25px] font-[400]">{item.desc}</p>
                        <div className="mt-[12px] flex items-center">
                            <p className="text-[#EFEFEF] text-[14px] leading-[20px] font-[500] tracking-[0.5px] after:w-[310px] after:h-[1px] after:bg-[#FFF] after:content-[''] after:block after:mt-[25px] after:opacity-30 ">Read More</p>
                            <img className="ms-[13px]" src={item.icon} alt="arrow-right" />
                        </div>
                    </div>                
                ))
            }
        </>
    )
}