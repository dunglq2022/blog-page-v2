export const ArtBlog = ({data}) => {
    return (  
        <>
            {
                data.map((item, index) => (
                    <div id="item__blogart" className="flex mb-[30px] bg-[#F4F7FF]" >
                    <div key={index} className="object-cover w-[370px] flex-shrink-0" >
                        <img className="w-[100%]" src={item.img} alt={item.title} />
                    </div>
                    <div className="p-[30px]">
                        <span className="text-blue-700 text-[14px] leading-[20px] font-[600] tracking-[0.5px]">{item.subject}</span>
                        <h2 className="mt-[7px] text-[#212B36] text-[22px] leading-[30px] font-[600]">{item.title}</h2>
                        <p className="mt-[12px] text-[#637381] text-[16px] leading-[25px] font-[400]">{item.desc}</p>
                        <div className="mt-[12px] flex items-center">
                            <p className="text-[#637381] text-[14px] leading-[20px] font-[500] tracking-[0.5px]">Read More</p>
                            <img className="ms-[13px]" src={item.icon} alt="arrow-right" />
                        </div>
                    </div>
                    </div>
                ))
            }
        </>
    )
}